var etat = 0; // variable qui donne l'etat du bouton MenuPlay pour savoir si l'utilisateur est sur le menu ou en jeu
// 0 : Menu     1 : Jeu
var dif = 0; // variable de la difficulté
// 0 : facile   1: normal   2: difficile

var btn_facile = document.getElementById("facile");
var btn_normal = document.getElementById("normal");
var btn_difficile = document.getElementById("difficile");
var btn_MenuJouer = document.getElementById("MenuJouer");

btn_MenuJouer.addEventListener("click", StopPlay);
btn_facile.addEventListener("click", Facile);
btn_normal.addEventListener("click", Normal);
btn_difficile.addEventListener("click", Difficile);

function StopPlay(){ 
    if (etat==0){// mode Menu
        etat += 1; 
        btn_MenuJouer.innerHTML = "Jouer";
        btn_facile.disabled = false;
        btn_normal.disabled = false;
        btn_difficile.disabled = false;
    }else{// mode en jeu
        etat -= 1;
        btn_MenuJouer.innerHTML = "Menu";
        btn_facile.disabled = true;
        btn_normal.disabled = true;
        btn_difficile.disabled = true;
    }
}

function Facile(){
    dif = 0;
    Select(dif);
}

function Normal(){
    dif = 1;
    Select(dif);
}

function Difficile(){
    dif = 2;
    Select(dif);
}

function Select(dif){
    if (dif == 0){
        //border
        btn_facile.style.border = "solid 3px red";
        btn_normal.style.border = "none";
        btn_difficile.style.border = "none";
        //color
        btn_facile.style.color = "red";
        btn_normal.style.color = "white";
        btn_difficile.style.color = "white";
    }else if (dif == 1){
        //border
        btn_facile.style.border = "none";
        btn_normal.style.border = "solid 3px red";
        btn_difficile.style.border = "none";
        //color
        btn_facile.style.color = "white";
        btn_normal.style.color = "red";
        btn_difficile.style.color = "white";
    }else if (dif == 2){
        //border
        btn_facile.style.border = "none";
        btn_normal.style.border = "none";
        btn_difficile.style.border = "solid 3px red";
        //color
        btn_facile.style.color = "white";
        btn_normal.style.color = "white";
        btn_difficile.style.color = "red";
    }
}

StopPlay();
Select(0);