let canvas = document.getElementById('myCanvas');
let ctx = canvas.getContext('2d');



// Grille
let WORLD = [
    ['EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
    ['EMPTY', 'EMPTY', 'EMPTY', 'FOOD',  'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
    ['EMPTY', 'HEAD', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
    ['EMPTY', 'SNAKE', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
    ['EMPTY', 'SNAKE', 'SNAKE', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
    ['EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
    ['EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
    ['EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
    ['EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
    ['EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY', 'EMPTY'],
];

// Le serpent
var player = [[4,2], [4,1], [3,1], [2,1]];

 console.log(WORLD);

//Fonction principale à l'execution du programme
 function main(){
    score = 0 //Initie le score à 0 pour le début de la partie
    drawWorld(); 
    var key = document.addEventListener("keydown",move); //Detecte la touche du clavier presser et l'envoie dans move()
    //var game = setInterval(500,move);
    
 }

 //Fonction qui dessine le monde et l'affiche à l'écran
 function drawWorld(){
    ctx.beginPath();
    ctx.strokeStyle = 'black';
    ctx.strokeRect(0,0,500,500); //Dessine les bordures extérieurs du terrains en noir

    //Pour chaque case, on teste son "contenu" et appelle la fonction drawCase() avec son contenu et ses coordonnées
    for (i=0;i<10;i++){
        for (j=0;j<10;j++){
            if (WORLD[i][j] == "EMPTY"){
                drawCase("EMPTY",i,j);    
            }
            else if (WORLD[i][j] == "FOOD"){
                drawCase("FOOD",i,j);
            }
            else if (WORLD[i][j] == "SNAKE"){
                drawCase("SNAKE",i,j);
            }
            else if (WORLD[i][j] == "HEAD")
                drawCase("HEAD",i,j);
        }
    }
 }

 //Dessine la case en fonction de son type et ses coordonnées
 function drawCase(typeCase,coorX,coorY){

    //Si la case est vide la colorie en blanc et dessine ces bordures en noir
    if (typeCase == "EMPTY"){ 
        ctx.fillStyle = "white";
        ctx.fillRect(coorY*50,coorX*50,50,50);
        ctx.strokeRect(coorY*50,coorX*50,50,50);

    }
    //Si la case contient de la nourriture, la colorie en rouge
    if (typeCase == 'FOOD'){
        ctx.fillStyle = '#ff9f00';
        ctx.fillRect(coorY*50,coorX*50,50,50);
    }

    //Si la case contient le serpent, la colorie en vert
    if (typeCase == 'SNAKE'){
        ctx.fillStyle = 'green';
        ctx.fillRect(coorY*50,coorX*50,50,50);
    }

    //Si la case contient la tete du serpent, la colorie en vert claire
    if (typeCase == 'HEAD'){
        ctx.fillStyle = '#00FF27';
        ctx.fillRect(coorY*50,coorX*50,50,50);
    }
 }

function step(key){
    var direction;
    if (key.code == "ArrowRight"){
        direction = "droite";
        move(direction);
    }
}

//Fonction qui fait déplacer le serpent 
function move(key){
    //console.log(key);
    snakeSize = player.length - 1;
    //console.log(snakeSize);

    //Réagit si on presse la flèche de droite ou "D"
    if (key.code == 'ArrowRight' || key.code == "KeyD"){
        
        console.log('droite');
        console.log(player);

        player.push([player[snakeSize][0],player[snakeSize][1]+1]);
        var oldCase = player.shift();       
        console.log(oldCase);

        WORLD[oldCase[0]][oldCase[1]] = "EMPTY";

        if (WORLD[player[snakeSize][0]][player[snakeSize][1]] == "SNAKE"){
            alert("Game Over");
        }
        
        if  (WORLD[player[snakeSize][0]][player[snakeSize][1]] == "FOOD"){
            
            WORLD[player[snakeSize][0]][player[snakeSize][1]] = "HEAD";
            WORLD[player[snakeSize][0]][player[snakeSize][1]-1] = "SNAKE";
            drawWorld();
            SizeSnake(oldCase);
            eatFood(); 
        }

        //console.log(WORLD)
        else{
            WORLD[player[snakeSize][0]][player[snakeSize][1]] = "HEAD"; 
            WORLD[player[snakeSize][0]][player[snakeSize][1]-1] = "SNAKE";  
        }
        drawWorld();
        
    }

    if (key.code == 'ArrowDown' || key.code == "KeyS"){
        console.log("bas");

        console.log(player);

        player.push([player[snakeSize][0]+1,player[snakeSize][1]]);
        var oldCase = player.shift();       
        console.log(oldCase);

        WORLD[oldCase[0]][oldCase[1]] = "EMPTY";

        if (WORLD[player[snakeSize][0]][player[snakeSize][1]] == "SNAKE"){
            alert("Game Over");
        }
        
        if  (WORLD[player[snakeSize][0]][player[snakeSize][1]] == "FOOD"){

            WORLD[player[snakeSize][0]][player[snakeSize][1]] = "HEAD"; 
            WORLD[player[snakeSize][0]-1][player[snakeSize][1]] = "SNAKE"; 
            SizeSnake(oldCase);
            eatFood();
            drawWorld();
        }

        
        else{
            WORLD[player[snakeSize][0]][player[snakeSize][1]] = "HEAD"; 
            WORLD[player[snakeSize][0]-1][player[snakeSize][1]] = "SNAKE"; 
        }

        drawWorld();
    }

    if (key.code == 'ArrowUp' || key.code == "KeyW"){
        console.log("haut");

        console.log(player);

        player.push([player[snakeSize][0]-1,player[snakeSize][1]]);
        var oldCase = player.shift();       
        console.log(oldCase);

        WORLD[oldCase[0]][oldCase[1]] = "EMPTY";

        if (WORLD[player[snakeSize][0]][player[snakeSize][1]] == "SNAKE"){
            alert("Game Over");
        }
        
        if  (WORLD[player[snakeSize][0]][player[snakeSize][1]] == "FOOD"){
            WORLD[player[snakeSize][0]][player[snakeSize][1]] = "HEAD"; 
            WORLD[player[snakeSize][0]+1][player[snakeSize][1]] = "SNAKE"; 
            drawWorld();
            SizeSnake(oldCase);
            eatFood();
            
        }

        else{
            WORLD[player[snakeSize][0]][player[snakeSize][1]] = "HEAD"; 
            WORLD[player[snakeSize][0]+1][player[snakeSize][1]] = "SNAKE"; 
        }
        
        
        drawWorld();
    }

    if (key.code == 'ArrowLeft' || key.code == "KeyA"){
        console.log('gauche');
        
        console.log(player);

        player.push([player[snakeSize][0],player[snakeSize][1]-1]);
        var oldCase = player.shift();       
        console.log(oldCase);

        WORLD[oldCase[0]][oldCase[1]] = "EMPTY";
        
        if (WORLD[player[snakeSize][0]][player[snakeSize][1]] == "SNAKE"){
            alert("Game Over");
        }

        if  (WORLD[player[snakeSize][0]][player[snakeSize][1]] == "FOOD"){
            WORLD[player[snakeSize][0]][player[snakeSize][1]] = "HEAD"; 
            WORLD[player[snakeSize][0]][player[snakeSize][1]+1] = "SNAKE"; 
            SizeSnake(oldCase);
            eatFood();
            drawWorld();
            
        }

        else{
            WORLD[player[snakeSize][0]][player[snakeSize][1]] = "HEAD"; 
            WORLD[player[snakeSize][0]][player[snakeSize][1]+1] = "SNAKE"; 
        }    

        drawWorld();   
    }
}

function eatFood(){

    i = 0;
    while (i == 0){
        
        //console.log("coucou");

        newFoodX = getRandomInt(10);
        newFoodY = getRandomInt(10);

        //console.log(newFoodX);
        //console.log(newFoodY);
        
        if (WORLD[newFoodY][newFoodX] == "EMPTY"){
            WORLD[newFoodY][newFoodX] = "FOOD";
           // console.log("Nouveau miam");
           // console.log(WORLD);
            drawWorld();
            i = i + 1; 
        }
    }

    

}

function SizeSnake(newTail){
    console.log("grandis");
    console.log(newTail);

    player.unshift(newTail);
    WORLD[newTail[0]][newTail[1]] = "SNAKE";
    console.log(WORLD);
    drawWorld();
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

 main()